import { AnyAction } from 'redux';

export interface AppState {
    isLoading: boolean;
}

const initialState: AppState = {
    isLoading: false
};

const prefix = 'app';

export const ActionTypes = {
    TEST: `${prefix}/test`
};

export default function reducer(state: AppState = initialState, action: AnyAction) {
    switch (action.type) {
        default:
            return state;
    }
}
