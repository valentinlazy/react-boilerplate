import { combineReducers } from 'redux';

import app, { AppState } from './app';
import auth, { AuthState } from './auth';

export default combineReducers({
    app,
    auth,
});

export interface RootState {
    app: AppState;
    auth: AuthState;
}
