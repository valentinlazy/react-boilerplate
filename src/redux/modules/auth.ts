import { Dispatch, AnyAction } from 'redux';

import { Credentials } from '../models';

// interfaces
export interface AuthState {
    isAuthenticated: boolean;
    error?: string | null;
    token: Token;
    isLoading: boolean;
}

export interface Token {
    access: string | null;
}

const prefix = 'auth';

export const ActionTypes = {
    LOGIN: `${prefix}/LOGIN`,
    LOGIN_SUCCESS: `${prefix}/LOGIN_SUCCESS`,
    LOGIN_FAIL: `${prefix}/LOGIN_FAIL`,
    LOGOUT: `${prefix}/LOGOUT`
};

const initialState: AuthState = {
    isAuthenticated: false,
    error: '',
    token: {
        access: null,
    },
    isLoading: false,
};

export function login(credentials: Credentials) {
    return (dispatch: Dispatch<AnyAction>) => {
        dispatch({ type: ActionTypes.LOGIN, payload: credentials });
        dispatch({ type: ActionTypes.LOGIN_SUCCESS, payload: {} });
        return Promise.resolve();
    };
}

export const logout = (inactive: boolean = false) => ({
    type: ActionTypes.LOGOUT,
    payload: inactive
});

export default function reducer(state: AuthState = initialState, action: AnyAction) {
    switch (action.type) {
        case ActionTypes.LOGIN:
            return {
                ...state,
                error: '',
                isAuthenticated: false,
                isLoading: true,
            };
        case ActionTypes.LOGIN_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isAuthenticated: true,
                token: {
                    access: action.payload.accessToken,
                }
            };
        case ActionTypes.LOGIN_FAIL:
            return {
                ...state,
                error: action.payload,
                token: null,
                isLoading: false,
            };
        case ActionTypes.LOGOUT:
            return initialState;
        default:
            return state;
    }
}
