import { createStore, applyMiddleware, compose } from 'redux';

import reducers, { RootState } from './modules';
import middlewares from './middlewares';

declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION__: Function;
    }
}

function cleanState(state: RootState) {
    return {
        ...state
    };
}

export function configureStore(initialState: Object = {}) {
    const enhancers = [
        applyMiddleware(...middlewares)
    ];

    if (process.env.NODE_ENV === 'development' &&
        typeof window.__REDUX_DEVTOOLS_EXTENSION__ === 'function') {
        enhancers.push(window.__REDUX_DEVTOOLS_EXTENSION__());
    }

    const savedStore = localStorage.getItem('store') && JSON.parse(localStorage.getItem('store') || '');

    if (savedStore) {
        initialState = savedStore;
    }

    return createStore(
        reducers,
        initialState,
        compose(...enhancers)
    );
}

export const store = configureStore();

store.subscribe(() =>  {
    const state = cleanState(store.getState() as RootState);
    localStorage.setItem('store', JSON.stringify(state));
});

export default store;
