import axios, { AxiosInstance } from 'axios';

import { getApiUrl } from '../constants';
import { RootState } from '../modules';

export function createThunkMiddleware (): any {
    return ({ dispatch, getState }: any) => (next: any) => (action: any) => {
        if (typeof action === 'function') {
            const { auth: { token } } = getState() as RootState;

            const axiosClient: AxiosInstance = axios.create({
                baseURL: getApiUrl(),
                responseType: 'json'
            });
            axiosClient.interceptors.request.use(
                (config: any): any => {
                    const authToken = `Bearer ${token.access}`;
                    config.headers = {...config.headers, 'Authorization': authToken};

                    return config;
                },
                (error: any): any => {
                    return Promise.reject(error);
                }
            );
            return action(dispatch, getState, axiosClient);
        }
        return next(action);
    };
}

const thunkWithAxiosMiddleware = createThunkMiddleware();

export default thunkWithAxiosMiddleware;
