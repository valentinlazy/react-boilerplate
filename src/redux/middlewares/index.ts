import { Middleware } from 'redux';

import thunkWithAxiosMiddleware from './thunkWithAxiosMiddleware';

export const middlewares: Array<Middleware> = [
    thunkWithAxiosMiddleware
];

export default middlewares;
