import * as React from 'react';

import * as styles from './index.css';

class Private extends React.Component {
    render() {
        return (
            <div className={styles.private}>
                <p>Private container</p>
            </div>
        );
    }
}

export default Private;
