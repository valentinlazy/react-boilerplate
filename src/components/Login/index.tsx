import * as React from 'react';
import { FormEvent } from 'react';

import * as styles from './login.css';

interface Props {
    login: any;
}

class Login extends React.Component<Props> {

    handleSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        /* tslint:disable:no-string-literal */
        const email = e.currentTarget.elements['email'].value;
        const password = e.currentTarget.elements['password'].value;
        // alert(`submitted credentials:
        // email: ${e.currentTarget.elements['email'].value}
        // password: ${e.currentTarget.elements['password'].value}
        // `);
        console.log(this.props);
        this.props.login({
            email,
            password
        });
    };

    render() {
        return (
            <div className={styles.app}>
                <form onSubmit={this.handleSubmit}>
                    <label>
                        Email:
                        <input name="email" type="text"/>
                    </label>
                    <label>
                        Password:
                        <input name="password" type="password"/>
                    </label>
                    <button type="submit">login</button>
                </form>
            </div>
        );
    }
}

export default Login;
