import * as React  from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';

import Login from '../../components/Login';
import { login } from '../../redux/modules/auth';
import { RootState } from '../../redux/modules';
import { Credentials } from '../../redux/models';

interface Props {
    login: (credentials: Credentials) => any;
    location: any;
    isAuthenticated: boolean;
}

class LoginContainer extends React.Component<Props> {
    render() {
        const { from } = this.props.location.state || { from: { pathname: '/' } };

        if (this.props.isAuthenticated) {
            return (<Redirect to={from} />);
        } else {
            return (<Login login={this.props.login} />);
        }
    }
}

const mapStateToProps = (state: RootState) => {
    return {
        isAuthenticated: state.auth.isAuthenticated
    };
};

const mapDispatchToProps = {
    login
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);
