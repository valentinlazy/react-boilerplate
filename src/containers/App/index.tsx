import * as React from 'react';
import { Route, Switch } from 'react-router';
import { BrowserRouter } from 'react-router-dom';

import Home from '../Home';
import Login from '../Login';
import Private from '../Private';
import PrivateRoute from '../Route/Private';

class App extends React.Component {
  public render() {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" exact={true} component={Home} />
                <Route path="/login" exact={true} component={Login} />
                <PrivateRoute path="/private" exact={true} component={Private} />
            </Switch>
        </BrowserRouter>
    );
  }
}

export default App;
