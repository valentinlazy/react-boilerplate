import * as React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import * as styles from './home.css';
import { RootState } from '../../redux/modules';
import { logout } from '../../redux/modules/auth';

interface Props {
    isAuthenticated: boolean;
    logout: any;
}

class HomeContainer extends React.Component<Props> {
    logout = () => {
        this.props.logout();
        return true;
    };

    render() {
        return (
            <div className={styles.app}>
                <header className={styles.header}>
                    <h1 className={styles.title}>Welcome to Inventory Manager</h1>
                </header>
                <Link to="/private">Private</Link> | &nbsp;
                {!this.props.isAuthenticated ?
                    <Link to="/login">go to login</Link> :
                    <a href="#" onClick={this.logout}>logout</a>
                }
            </div>
        );
    }
}

const mapStateToProps = (state: RootState) => {
    return {
        isAuthenticated: state.auth.isAuthenticated
    };
};

const mapDispatchToProps = {
    logout
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);
