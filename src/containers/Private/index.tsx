import * as React from 'react';
import Private from '../../components/Private';

class PrivateContainer extends React.Component {
    render() {
        return (
            <Private />
        );
    }
}

export default PrivateContainer;
