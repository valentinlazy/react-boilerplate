import * as React from 'react';
import { Route, Redirect, RouteProps } from 'react-router';
import { connect } from 'react-redux';

import { RootState } from '../../redux/modules';

interface Props {
    component: Function;
    isAuthenticated: boolean;
}

class PrivateRouteContainer extends Route<Props & RouteProps> {
    render() {
        const { component: Component, isAuthenticated, ...rest } = this.props;
        return (
            <Route
                {...rest}
                render={props =>
                    isAuthenticated ? (
                        <Component {...props} />
                    ) : (
                        <Redirect
                            to={{
                                pathname: '/login',
                                state: {from: props.location}
                            }}
                        />
                    )
                }
            />
        );
    }
}

const mapStateToProps = (state: RootState) => {
    return {
        isAuthenticated: state.auth.isAuthenticated
    };
};

export default connect(mapStateToProps)(PrivateRouteContainer);
